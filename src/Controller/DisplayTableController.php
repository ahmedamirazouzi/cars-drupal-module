<?php

namespace Drupal\cars\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Url;


class DisplayTableController extends ControllerBase {



  public function display() {

    //create table header
    $header_table = array(
     'id'=>    t('No'),
      'name' => t('Name'),
        'matricule' => t('Matricule'),
        'color' => t('Color'),

        'opt' => t('operations'),
        'opt1' => t('operations'),
    );

//select records from table
    $query = \Drupal::database()->select('cars', 'c');
      $query->fields('c', ['id','name','matricule','color']);
      $results = $query->execute()->fetchAll();
        $rows=array();
    foreach($results as $data){
        $delete = Url::fromUserInput('/cars/form/delete/'.$data->id);
        $edit   = Url::fromUserInput('/cars/form/cars?num='.$data->id);

      //print the data from table
             $rows[] = array(
            'id' =>$data->id,
                'name' => $data->name,
                'matricule' => $data->matricule,
                'color' => $data->color,

                 \Drupal::l('Delete', $delete),
                 \Drupal::l('Edit', $edit),
            );

    }
    //display data in site
    $form['table'] = [
            '#type' => 'table',
            '#header' => $header_table,
            '#rows' => $rows,
            '#empty' => t('No users found'),
        ];
        return $form;

  }

}
