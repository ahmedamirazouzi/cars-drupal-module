<?php

namespace Drupal\cars\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\Core\Database\Database;
use Symfony\Component\HttpFoundation\RedirectResponse;


class CarsForm extends FormBase {



  public function getFormId() {
    return 'cars_form';
  }

 
  public function buildForm(array $form, FormStateInterface $form_state) {

    $conn = Database::getConnection();
     $record = array();
    if (isset($_GET['num'])) {
        $query = $conn->select('cars', 'c')
            ->condition('id', $_GET['num'])
            ->fields('c');
        $record = $query->execute()->fetchAssoc();

    }

    $form['car_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Car Name:'),
      '#required' => TRUE,
      '#default_value' => (isset($record['name']) && $_GET['num']) ? $record['name']:'',
      );
    //print_r($form);die();

    $form['car_matricule'] = array(
      '#type' => 'textfield',
      '#title' => t('Car Matricule:'),
      "#required" => TRUE,
      '#default_value' => (isset($record['matricule']) && $_GET['num']) ? $record['matricule']:'',
      );

    $form['car_color'] = array (
      '#type' => 'textfield',
      '#title' => t('Color'),
      '#required' => TRUE,
      '#default_value' => (isset($record['color']) && $_GET['num']) ? $record['color']:'',
       );


    $form['submit'] = [
        '#type' => 'submit',
        '#value' => 'Save',
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {

    $field=$form_state->getValues();
    $name=$field['car_name'];
    $matricule=$field['car_matricule'];
    $color=$field['car_color'];


    if (isset($_GET['num'])) {
          $field  = array(
              'name'   => $name,
              'matricule' =>  $matricule,
              'color' =>  $color,
          );
          $query = \Drupal::database();
          $query->update('cars')
              ->fields($field)
              ->condition('id', $_GET['num'])
              ->execute();
          drupal_set_message("succesfully updated");
          $form_state->setRedirect('cars.display_table_controller_display');

      }

       else
       {
           $field  = array(
              'name'   =>  $name,
              'matricule' =>  $matricule,
              'color' =>  $color,
          );
           $query = \Drupal::database();
           $query ->insert('cars')
               ->fields($field)
               ->execute();
           drupal_set_message("succesfully saved");

           $form_state->setRedirect('cars.display_table_controller_display');
       }
     }

}
